class GreetingCard:
    def __init__(self, recipient="Dana Ev", sender="Eyal Ch"):
        """ Initializes a GreetingCard object.
        :param recipient: The recipient of the greeting card. Default is "Dana Ev".
        :type recipient: str
        :param sender: The sender of the greeting card. Default is "Eyal Ch".
        :type sender: str
        """
        self._recipient = recipient
        self._sender = sender

    def greeting_msg(self):
        """
        Prints the greeting message with the sender and recipient information.
        """
        print("The sender: %s, the recipient: %s" % (self._sender, self._recipient))
