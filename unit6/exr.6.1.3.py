import tkinter as tk

window = tk.Tk()
label = tk.Label(window, text="whats my favorite video?", fg="pink", font=("Arial", 16))
label.pack()

def show_image():
    #the function adding the given image to the image_label
    # Load the image
    image = tk.PhotoImage(file="button_img.png")
    resized_image = image.subsample(2)  # Adjust the size to its half

    image_label.config(image=resized_image)
    image_label.image = resized_image


button = tk.Button(window, text="Click to find out!", command=show_image)
button.pack()

image_label = tk.Label(window)
image_label.pack()

window.mainloop()
