from file1 import GreetingCard

class BirthdayCard(GreetingCard):
    def __init__(self, age=0, recipient="Dana Ev", sender="Eyal Ch"):
        """ Initializes a BirthdayCard object, inheriting from GreetingCard.
        :param age: The age of the sender. Default is 0.
        :type age: int
        :param recipient: The recipient of the birthday card. Default is "Dana Ev".
        :type recipient: str
        :param sender: The sender of the birthday card. Default is "Eyal Ch".
        :type sender: str
        """
        super().__init__(recipient, sender)
        self._sender_age = age

    def greeting_msg(self):
        """
        Prints the birthday greeting message with the sender, recipient, and sender's age information.
        """
        print("Happy birthday!")
        print("The sender: %s, the recipient: %s" % (self._sender, self._recipient))
        print("The sender age is", self._sender_age)
