def get_fibo():
    """ Generates Fibonacci numbers (starting from 0 and 1).
    :yield: The Fibonacci numbers in sequence.
    :rtype: generator
    """
    first, second = 0, 1

    while True:
        temp = second
        second = first + second
        yield first
        first = temp

fibo_gen = get_fibo()
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
