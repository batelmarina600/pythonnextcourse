def first_prime_over(n):
    """ Returns the first prime number that is greater than n.
    :param n: The number after which to find the prime number.
    :type n: int
    :return: The first prime number greater than n.
    :rtype: int
    """
    prime_gen = (number for number in range(n + 1, 2 * n + 1) if(is_prime(number)))
    return next(prime_gen)


def is_prime(n):
    """Checks if a number is prime.
    :param n: The number to check.
    :type n: int
    :return: True if the number is prime, False otherwise.
    :rtype: bool
    """
    # Corner case
    if n <= 1:
        return False
    # Check from 2 to n-1
    for i in range(2, n):
        if n % i == 0:
            return False
    return True

print(first_prime_over(1000000))
