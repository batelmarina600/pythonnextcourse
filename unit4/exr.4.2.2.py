def parse_ranges(ranges_string):
    """ Parses the ranges_string and generates individual numbers from the specified ranges.
    :param ranges_string: The string containing number ranges separated by commas.
    :type ranges_string: str
    :yield: A generator that produces individual numbers from the specified ranges.
    :rtype: generator
    """
    input_gen = (word for word in ranges_string.split(","))
    split_gen = (word.split("-") for word in input_gen)
    res_gen = (number for start,end in split_gen for number in range(int(start), int(end) + 1))
    return res_gen
        

print(list(parse_ranges("1-2,4-4,8-10")))
print(list(parse_ranges("0-0,4-8,20-21,43-45")))