def gen_secs():
    """ Generates seconds from 0 to 59.
    :yield: The seconds in sequence.
    :rtype: generator
    """
    return (sec for sec in range(60))
    
def gen_minutes():
    """ Generates minutes from 0 to 59.
    :yield: The minutes in sequence.
    :rtype: generator
    """
    return (min for min in range(60))

def gen_hours():
    """ Generates hours from 0 to 23.
    :yield: The hours in sequence.
    :rtype: generator
    """
    return (hour for hour in range(24))

def gen_time():
    """ Generates time strings in the format "HH:MM:SS".
    :yield: The time strings in sequence.
    :rtype: generator
    """
    for hour in gen_hours():
        for minute in gen_minutes():
            for second in gen_secs():
                time_str = "%02d:%02d:%02d" % (hour, minute, second)
                yield time_str

def gen_years(start=2023):
    """ Generates years starting from a specified year.
    :param start: The starting year.
    :type start: int
    :yield: The years in sequence.
    :rtype: generator
    """
    while start <= 2023:
        yield start
        start += 1

def gen_months():
    """ Generates months from 1 to 12.
    :yield: The months in sequence.
    :rtype: generator
    """
    return (month for month in range(1, 13))

def gen_days(month, leap_year=True):
    """ Generates days of a month.
    :param month: The month.
    :type month: int
    :param leap_year: Flag indicating if the year is a leap year, defaults to True.
    :type leap_year: bool, optional
    :yield: The days of the specified month in sequence.
    :rtype: generator
    """
    days_per_month = {
        1: 31,
        2: 29 if leap_year else 28,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31
    }
    return (day for day in range(1, days_per_month[month] + 1))


def gen_date():
    """ Generates date strings in the format "DD/MM/YYYY HH:MM:SS".
    :yield: The date strings in sequence.
    :rtype: generator
    """
    for year in gen_years(2019):
        for month in gen_months():
            for day in gen_days(month, year % 4 == 0):
                for time in gen_time():
                    date_str = "%02d/%02d/%04d %s" % (day, month, year, time)
                    yield date_str


cnt = 1
dates = gen_date()
while True:
    date = next(dates)
    if(cnt % 1000000 == 0):
        print(date)
    cnt += 1
