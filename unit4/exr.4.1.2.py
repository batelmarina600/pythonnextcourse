def translate(sentence):
    """ Translates the words in the given sentence based on the provided dictionary of words.
    :param sentence: The sentence to translate.
    :type sentence: str
    :return: The translated sentence.
    :rtype: str
    """
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    word_gen = (words[word] for word in sentence.split(" ") if word in words)
    result = " ".join(word_gen)
    return result

print(translate("el gato esta en la casa"))
