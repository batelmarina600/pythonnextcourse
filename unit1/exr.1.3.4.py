def find_password(password):
    """the function receives an encrypted password and needs to decrypt it, 
    the encryption: each letter shifted by two places
    :param password: the encrypted password
    :type password: string
    :return: the decrypt password
    :rtype: string
    """
    return "".join([chr(ord(char) + 2) for char in password])

password = "sljmai ugrf rfc ambc: lglc dmsp mlc rum"
print(find_password(password))
