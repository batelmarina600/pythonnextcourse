import functools

def longest_name(data):
    """ the function returns the longest name from the given data.
    :param data: A list of names.
    :type data: list
    :return: The longest name in the data.
    :rtype: str
    """
    cnt = max([len(name) for name in data])
    return "".join([name for name in data if len(name) == cnt])

def sum_len(data):
    """ the function calculates the sum of the lengths of strings in the given data.
    :param data: A list of strings.
    :type data: list
    :return: The sum of the lengths of strings in the data.
    :rtype: int
    """
    return functools.reduce(lambda x,y: x + len(y), data, 0)

def shortest_names(data):
    """ the function returns the names with the shortest length from the given data.
    :param data: A list of names.
    :type data: list
    :return: Names with the shortest length, separated by newlines.
    :rtype: str
    """
    cnt = min([len(name) for name in data])
    return "\n".join([name for name in data if len(name) == cnt])

def length_file(data):
    """ the function writes the lengths of the names in the given data to a file.
    :param data: A list of names.
    :type data: list
    :return: None
    """
    len_data = "\n".join([str(len(name)) for name in data])
    with open("name_length.txt", "w") as new_file:
        new_file.write(len_data)

def input_len_names(data, cnt):
    """ the function returns the names from the given data with the specified length.
    :param data: A list of names.
    :type data: list
    :param cnt: The desired length of names.
    :type cnt: int
    :return: Names with the specified length, separated by newlines.
    :rtype: str
    """
    return "\n".join([name for name in data if len(name) == cnt])

def main():
    file = open("text.txt", "r")
    data = file.read()
    data = data.split("\n")

    print(longest_name((data))) #1
    #print(sum_len(data)) #2
    #print(shortest_names(data)) #3

    #length_file(data) #4
    #with open("name_length.txt", "r") as new_file: #to check #4:
    #    print(new_file.read())

    #input_len = int(input("Enter name length: ")) #5
    #print(input_len_names(data, input_len))

if __name__=="__main__":
    main()