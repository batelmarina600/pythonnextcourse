def intersection(list_1, list_2):
    """The function accepts two lists and returns a list of the members that are in the intersection between them.
    That is, members that are in both the first list and the second list. 
    No member will appear twice in the cut list.
    :param list_1: the first list that the function going to search on
    :param list_2: the secind list that the function going to search on
    :type list_1: list
    :type list_2: list
    :return: a list of the members that are in the intersection between them
    :rtype: list
    """
    return list(set([x for x in list_1 for y in list_2 if x == y]))

print(intersection([1, 2, 3, 4], [8, 3, 9]))
print(intersection([5, 5, 6, 6, 7, 7], [1, 5, 9, 5, 6]))
