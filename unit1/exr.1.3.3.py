def is_funny(string):
    """the function receives a string and returns true only if the received string is composed entirely of 
    the characters 'h' and 'a'
    :param string: the string that the function going to search on
    :type string: string
    :return: True if has just 'h' and 'a', otherwise False
    :rtype: bool
    """
    return len([char for char in string if char != 'h' and char != 'a']) == 0

print(is_funny("hahahahahaha"))