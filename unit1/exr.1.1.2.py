def double_char(char):
    """ the function doubles the given character.
    :param char: The character to double.
    :type char: str
    :return: The doubled character.
    :rtype: str
    """
    return char + char

def double_letter(my_str):
    """ the function doubles each letter in the given string.
    :param my_str: The string to double the letters.
    :type my_str: str
    :return: The resulting string with doubled letters.
    :rtype: str
    """
    return "".join(map(double_char, my_str))

print(double_letter("python"))
print(double_letter("we are the champions!"))



"""The function returns a new list containing all the elements that are greater than the number n.
    :param my_list: list that going to searched on
    :param n: a number, checking the numbers in the list will be relative to this number
    :type my_list: list
    :type n: int
    :return: list containing all the items in my_list who are bigger than n
    :rtype: list
    """

""" 
    :param my_str: the string that the function going to search on
    :type my_str: string
    :return: 
    :rtype: string
    """