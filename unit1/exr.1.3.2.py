def is_prime(number):
    """The function accepts a number and returns a boolean value representing whether it is prime . 
    A prime number is a number that is divisible by itself and 1 without a remainder.
    :param number: the number that going to be checked
    :type number: int
    :return: True- if its prime, False- if its not prime
    :rtype: bool
    """
    return len([x for x in range(2, int(number / 2) + 1) if number % x == 0]) == 0

print(is_prime(42))
print(is_prime(43))
