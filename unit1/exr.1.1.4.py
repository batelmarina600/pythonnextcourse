def sum_of_digits(number):
    """The function accepts a number and returns the sum of its digits.
    :param number: the number that the function going to work on
    :type number: int
    :return: digits sum
    :rtype: int
    """
    return sum(map(int, str(number)))

print(sum_of_digits(104))