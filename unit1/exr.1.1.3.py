def divide_by_four(number):
    """ the function checks if the given number is divisible by four.
    :param number: The number to check.
    :type number: int
    :return: True if the number is divisible by four, False otherwise.
    :rtype: bool
    """
    return number % 4 == 0

def four_dividers(number):
    """ the function eturns a list of numbers from 1 up to the given number that are divisible by four.
    :param number: The upper limit of the range.
    :type number: int
    :return: List of numbers divisible by four.
    :rtype: list
    """
    return list(filter(divide_by_four, range(1, number)))

print(four_dividers(9))
print(four_dividers(3))