import itertools 
wallet = [20, 20, 20, 10, 10, 10, 10, 10, 5, 5, 1, 1, 1, 1, 1]
target_sum = 100

combinations = []
for length in range(1, len(wallet) + 1): #going over different lengths
    for combination in itertools.combinations(wallet, length): # Find combinations that sum up to the target sum
         if sum(combination) == target_sum:
            combinations.append(combination)

final_combinations = set(combinations)  # Remove duplicates

for combination in final_combinations:
    print(sorted(combination))

print("number of combinations: ", len(final_combinations))

