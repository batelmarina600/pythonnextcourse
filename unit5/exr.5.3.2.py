class MusicNotes:
    """ Iterates over musical notes and their frequencies.
    Each iteration returns the next note frequency based on the defined note sequence.
    """

    def __init__(self):
        """
        Initializes the MusicNotes iterator.
        """
        self.notes = [("La", 55), ("Si", 61.74), ("Do", 65.41), ("Re", 73.42), ("Mi", 82.41), ("Fa", 87.31), ("Sol", 98)]
        self.note_index = 0
        self.octave = 0

    def __iter__(self):
        """ Returns the iterator object itself.
        :return: The MusicNotes iterator object.
        :rtype: MusicNotes
        """
        return self

    def __next__(self):
        """ Returns the next note frequency in the iteration.
        Raises StopIteration if the octave exceeds the maximum limit.
        :return: The next note frequency.
        :rtype: float
        """
        if self.note_index >= len(self.notes):
            self.note_index = 0
            self.octave += 1

        if self.octave > 4:
            raise StopIteration

        frequency = self.notes[self.note_index][1]
        frequency *= 2 ** self.octave

        self.note_index += 1
        return frequency


music_notes = iter(MusicNotes())
for note in music_notes:
    print(note)
