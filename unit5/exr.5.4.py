class IDIterator:
    """
    An iterator that generates valid ID numbers based on the Luhn algorithm.
    """

    def __init__(self, id):
        """ Initializes the IDIterator with the starting ID.
        :param id: The starting ID number.
        :type id: int
        """
        self._id = id

    def __iter__(self):
        """ Returns the iterator object itself.
        :return: The iterator object.
        :rtype: IDIterator
        """
        return self
    
    def __next__(self):
        """ Returns the next valid ID number by incrementing the current ID until a valid ID is found.
        :return: The next valid ID number.
        :rtype: int
        :raises: StopIteration: When the maximum ID number (999999999) is reached.
        """
        while not check_id_valid(self._id): #until a valid id
            self._id += 1
            if self._id == 999999999: #got to the end
                raise StopIteration
        self._id += 1
        return self._id - 1


def check_id_valid(id_number):
    """
    Checks if the given ID number is valid based on the Luhn algorithm.

    :param id_number: The ID number to check.
    :type id_number: int
    :return: True if the ID number is valid, False otherwise.
    :rtype: bool
    """
    i = 1
    total_sum = 0
    for digit_char in str(id_number):
        digit = int(digit_char)
        if i % 2 == 0: # multiply by two the even places
            digit *= 2 
        i += 1
        if digit > 9: #if two digits- sum the digits
            digit = digit % 10 + (digit // 10)
        total_sum += digit

    if total_sum % 10 == 0:
        return True
    else:
        return False


def id_generator(id):
    """
    Generates valid ID numbers starting from the given ID.

    :param id: The starting ID number.
    :type id: int
    :yield: The next valid ID number.
    :rtype: int
    :raises StopIteration: When the maximum ID number (999999999) is reached.
    """
    while id < 999999999:
        if check_id_valid(id):
            yield id
        id += 1
    raise StopIteration


def main():
    input_num = int(input("Enter ID: "))
    input_str = input("Generator or Iterator? (gen/it)? ")

    if input_str == "it":
        id_iterator = iter(IDIterator(input_num))
        for i in range(10):
            print(next(id_iterator))
            
    elif input_str == "gen":
        id_gen = id_generator(input_num)
        for i in range(10):
            print(next(id_gen))


if __name__ == "__main__":
    main()
