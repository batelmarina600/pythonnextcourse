def combine_coins(coin, numbers):
    """Combines a coin symbol with each number in the input list and returns a string representation.
    :param coin: The coin symbol to combine with the numbers.
    :param numbers: The list of numbers to combine with the coin symbol.
    :type coin: str
    :type numbers: list
    :return: The combined string representation of the coin symbol and numbers.
    :rtype: str
    """
    return "".join([coin + str(number) + ", " for number in numbers])[:-2]
                

print(combine_coins('$',range(5)))

