def read_file(file_name):
    """ Reads the contents of a file and returns it as a string.
    :param file_name: The name of the file to be read.
    :type file_name: str
    :return: The contents of the file or an error message if the file does not exist.
    :rtype: str
    """
    str= ""
    try:
        str += "__CONTENT_START__\n"
        f = open(file_name, "r")
    except:
        str += "__NO_SUCH_FILE__"
    else:
        str += f.read()
    finally:
        str += "\n__CONTENT_END__\n"
        return str

print(read_file("one_lined_file.txt"))
print(read_file("file_does_not_exist.txt"))

