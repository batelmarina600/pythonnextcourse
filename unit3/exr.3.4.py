import string

class UsernameContainsIllegalCharacter(Exception):
    """
    Exception raised when the username contains an illegal character at a specific index.
    """

    def __init__(self, character, index):
        """ Initializes the exception with the illegal character and its index.
        :param character: The illegal character.
        :type character: str
        :param index: The index where the illegal character is found.
        :type index: int
        """
        self.character = character
        self.index = index

    def __str__(self):
        """ Returns a string representation of the exception.
        :return: The string representation.
        :rtype: str
        """
        return f"Username contains illegal character '{self.character}' at index {self.index}"


class UsernameTooShort(Exception):
    """
    Exception raised when the username is too short.
    """

    def __str__(self):
        """ Returns a string representation of the exception.
        :return: The string representation.
        :rtype: str
        """
        return "Username is too short"


class UsernameTooLong(Exception):
    """
    Exception raised when the username is too long.
    """

    def __str__(self):
        """ Returns a string representation of the exception.
        :return: The string representation.
        :rtype: str
        """
        return "Username is too long"


class PasswordMissingCharacter(Exception):
    """
    Base exception class for password-related errors.
    """

    def __str__(self):
        """ Returns a string representation of the exception.
        :return: The string representation.
        :rtype: str
        """
        return "Password is missing a character."


class PasswordMissingUppercase(PasswordMissingCharacter):
    """
    Exception raised when the password is missing an uppercase letter.
    """

    def __str__(self):
        """ Returns a string representation of the exception.

        :return: The string representation.
        :rtype: str
        """
        return "Password is missing an uppercase letter."


class PasswordMissingLowercase(PasswordMissingCharacter):
    """
    Exception raised when the password is missing a lowercase letter.
    """

    def __str__(self):
        """
        Returns a string representation of the exception.

        :return: The string representation.
        :rtype: str
        """
        return "Password is missing a lowercase letter."

class PasswordMissingDigit(PasswordMissingCharacter):
    """
    Exception raised when the password is missing a digit.
    """

    def __str__(self):
        """
        Returns a string representation of the exception.

        :return: The string representation.
        :rtype: str
        """
        return "Password is missing a digit."


class PasswordMissingSpecial(PasswordMissingCharacter):
    """
    Exception raised when the password is missing a special character.
    """

    def __str__(self):
        """
        Returns a string representation of the exception.

        :return: The string representation.
        :rtype: str
        """
        return "Password is missing a special character."


class PasswordTooShort(Exception):
    """
    Exception raised when the password is too short.
    """

    def __str__(self):
        """
        Returns a string representation of the exception.

        :return: The string representation.
        :rtype: str
        """
        return "Password is too short"


class PasswordTooLong(Exception):
    """
    Exception raised when the password is too long.
    """

    def __str__(self):
        """
        Returns a string representation of the exception.

        :return: The string representation.
        :rtype: str
        """
        return "Password is too long"


def check_input(username, password):
    """
    Checks if the username and password meet the specified criteria.

    :param username: The username to check.
    :type username: str
    :param password: The password to check.
    :type password: str
    """
    try:
        if not all(c.isalnum() or c == '_' for c in username):
            for i, c in enumerate(username):
                if not c.isalnum() and c != '_':
                    raise UsernameContainsIllegalCharacter(c, i)
        if len(username) < 3:
            raise UsernameTooShort()
        if len(username) > 16:
            raise UsernameTooLong()

        if len(password) < 8:
            raise PasswordTooShort()
        if len(password) > 40:
            raise PasswordTooLong()

        if not any(c.isupper() for c in password):
            raise PasswordMissingUppercase()
        if not any(c.islower() for c in password):
            raise PasswordMissingLowercase()
        if not any(c.isdigit() for c in password):
            raise PasswordMissingDigit()
        if not any(c in string.punctuation for c in password):
            raise PasswordMissingSpecial()

        print("OK")

    except UsernameContainsIllegalCharacter as e:
        print(e)

    except UsernameTooShort as e:
        print(e)

    except UsernameTooLong as e:
        print(e)

    except PasswordTooShort as e:
        print(e)

    except PasswordTooLong as e:
        print(e)

    except PasswordMissingUppercase as e:
        print(e)

    except PasswordMissingLowercase as e:
        print(e)

    except PasswordMissingDigit as e:
        print(e)

    except PasswordMissingSpecial as e:
        print(e)


def main():
    """
    The main function that demonstrates the usage of the check_input function.
    """
    check_input("A_a1.", "12345678")
    check_input("A_1", "abcdefghijklmnop")
    check_input("A_1", "ABCDEFGHIJLKMNOP")
    check_input("A_1", "ABCDEFGhijklmnop")
    check_input("A_1", "4BCD3F6h1jk1mn0p")


if __name__ == "__main__":
    main()
