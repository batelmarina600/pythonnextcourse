def throw_stop_iteration():
    iterator = iter([]) #create an empty iterator
    next(iterator)  # Try to get the next element from the iterator, but there are no elements


def throw_zero_division_error():
    result = 5 / 0

def throw_assertion_error():
    x = 3
    y = 4
    assert x > y, "x should be greater than y"

def throw_import_error():
    #import non_existent_module
    print("remove the line before to have the error")

def throw_key_error():
    my_dict = {"key1": "value1"}
    value = my_dict["non_existent_key"]

def throw_syntax_error():
    eval("x = 5 +")

def throw_indentation_error():
    if True:
    #print("This line is not indented correctly")
        print("remove the line before to have the error")

def throw_type_error():
    x = "5"
    y = 10
    result = x + y

def main():
    throw_stop_iteration()
    #throw_zero_division_error()
    #throw_assertion_error()
    #throw_import_error()
    #throw_key_error()
    #throw_syntax_error()
    #throw_indentation_error()
    #throw_type_error()

if __name__=="__main__":
    main()