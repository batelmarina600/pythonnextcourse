class UnderAge(Exception):
    """
    Custom exception class to handle the case of underage individuals.
    """

    def __init__(self, age):
        """ Initializes an instance of the UnderAge exception.
        :param age: The age of the individual.
        :type age: int
        """
        self.age = age

    def __str__(self):
        """Returns a string representation of the exception.
        :return: The string representation of the exception.
        :rtype: str
        """
        return "Their age is lower than 18, it's %s and in %s years they can come" % (self.age, 18 - self.age)


def send_invitation(name, age):
    """ Sends an invitation to an individual based on their age.
    :param name: The name of the individual.
    :type name: str
    :param age: The age of the individual.
    :type age: str
    """
    try:
        if int(age) < 18:
            raise UnderAge(int(age))
    except UnderAge as E:
        print(E.__str__())
    else:
        print("You should send an invite to " + name)


send_invitation("Harry", 17)
send_invitation("Pete", 20)
