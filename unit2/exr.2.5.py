class Animal:
    "A class representing an animal"

    zoo_name = "Hayaton"

    def __init__(self, name, hunger=0):
        """
        Initializes a new instance of the Animal class.

        :param name: The name of the animal.
        :param hunger: The hunger level of the animal (default is 0).
        :type name: str
        :type hunger: int
        """
        self._name = name
        self._hunger = hunger
    
    def get_name(self):
        """Returns the name of the animal.
        :return: The name of the animal.
        :rtype: str
        """
        return self._name
    
    def is_hungry(self):
        """ Checks if the animal is hungry.
        :return: True if the animal is hungry, False otherwise.
        :rtype: bool
        """
        return self._hunger > 0
    
    def feed(self):
        "Reduces the hunger level of the animal by 1."
        self._hunger -= 1

    def talk(self):
        "An abstract method for making the animal talk."
        pass

class Dog(Animal):
    "A class representing a dog, which is a type of animal."

    def __init__(self, name, hunger=0):
        """
        Initializes a new instance of the Dog class.

        :param name: The name of the dog.
        :type name: str
        :param hunger: The hunger level of the dog (default is 0).
        :type hunger: int
        """
        super().__init__(name, hunger)
    
    def talk(self):
        "Prints the sound of a dog barking."
        print("woof woof")

    def fetch_stick(self):
        "Prints a message indicating that the dog is fetching a stick."
        print("There you go, sir!")


class Cat(Animal):
    "A class representing a cat, which is a type of animal."

    def __init__(self, name, hunger=0):
        """Initializes a new instance of the Cat class.
        :param name: The name of the cat.
        :type name: str
        :param hunger: The hunger level of the cat (default is 0).
        :type hunger: int
        """
        super().__init__(name, hunger)
    
    def talk(self):
        "Prints the sound of a cat meowing."
        print("meow")

    def chase_laser(self):
        "prints a message indicating that the cat is chasing a laser."
        print("Meeeeow")


class Skunk(Animal):
    "A class representing a skunk, which is a type of animal."

    def __init__(self, name, hunger=0):
        """
        Initializes a new instance of the Skunk class.

        :param name: The name of the skunk.
        :type name: str
        :param hunger: The hunger level of the skunk (default is 0).
        :type hunger: int
        """
        super().__init__(name, hunger)
        self._stink_count = 6
    
    def talk(self):
       "Prints the sound of a skunk hissing."
       print("tsssss")

    def stink(self):
        "Prints a message indicating that the skunk is releasing a smell."
        print("Dear lord!")
    

class Unicorn(Animal):
    " A class representing a unicorn, which is a type"
    def __init__(self, name, hunger=0):
        """ Initializes a new instance of the Unicorn class.
        :param name: The name of the unicorn.
        :type name: str
        :param hunger: The hunger level of the unicorn (default is 0).
        :type hunger: int
        """
        super().__init__(name, hunger)
    
    def talk(self):
        "Prints the sound of a unicorn speaking."
        print("Good day, darling")

    def sing(self):
        "Prints a message indicating that the unicorn is singing."
        print("I’m not your toy...")

class Dragon(Animal):
    "A class representing a dragon, which is a type of animal."

    def __init__(self, name, hunger=0):
        """ Initializes a new instance of the Dragon class.
        :param name: The name of the dragon.
        :type name: str
        :param hunger: The hunger level of the dragon (default is 0).
        :type hunger: int
        """
        super().__init__(name, hunger)
        self._color = "Green"

    def talk(self):
        "Prints the sound of a dragon roaring."
        print("Raaaawr")
    
    def breath_fire(self):
        "Prints a message indicating that the dragon is breathing fire."
        print("$@#$#@$")


def special_action(animal):
    """Performs a special action based on the type of animal.
    :param animal: The animal object.
    :type animal: Animal
    """
    if(isinstance(animal, Dog)):
        animal.fetch_stick()
    if(isinstance(animal, Cat)):
        animal.chase_laser()
    if(isinstance(animal, Skunk)):
        animal.stink()
    if(isinstance(animal, Unicorn)):
        animal.sing()
    if(isinstance(animal, Dragon)):
        animal.breath_fire()


def main():
    dog1 = Dog("Brownie", 10)
    cat1 = Cat("Zelda", 3)
    skunk1 = Skunk("Stinky")
    unicorn1 = Unicorn("Keith", 7)
    dragon1 = Dragon("Lizzy", 1450)

    dog2 = Dog("Doggo", 80)
    cat2 = Cat("Kitty", 80)
    skunk2 = Skunk("Stinky Jr.", 80)
    unicorn2 = Unicorn("Clair", 80)
    dragon2 = Dragon("McFly", 80)

    zoo_lst = [dog1, cat1, skunk1, unicorn1, dragon1, dog2, cat2, skunk2, unicorn2, dragon2]

    print("Welcom to the zoo:", Animal.zoo_name, "\n")
    for animal in zoo_lst:
        if(animal.is_hungry()):
            print(animal.__class__.__name__, animal.get_name())
            while animal.is_hungry():
                animal.feed()
        animal.talk()
        special_action(animal)

if __name__=="__main__":
    main()