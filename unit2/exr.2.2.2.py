class Bear:
   
    def __init__(self): #initialize
        self.name = "Beri"
        self.age = 0
    
    def birthday(self):
        "adds one to the age"
        self.age += 1
    
    def get_age (self):
        "returns the self's age"
        return self.age
    

def main():
    bear1 = Bear()
    bear2 = Bear()
    bear1.birthday()
    print("bear1's age: ", bear1.get_age())
    print("bear2's age: ", bear2.get_age())

if __name__=="__main__":
    main()