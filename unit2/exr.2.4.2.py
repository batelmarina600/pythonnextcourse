class BigThing:

    def __init__(self, param): #initialize
        if(isinstance(param, int)): #if its int
            self.the_size = param
        elif (isinstance(param, str) or isinstance(param, list) or isinstance(param, dict)): #if its str/list/dict
            self.the_size = len(param)

    def size(self):
        "return the self's size"
        return self.the_size
    
class BigCat(BigThing):

    def __init__(self, param, weight): #initialize
        super().__init__(param)
        self.weight = weight

    def size(self):
        """returns ok if the self's size is less than 15, if it between 15-20 returns Fat 
        and if more than 20-returns Very Fat"""

        if(self.weight < 15):
            return "OK"
        elif self.weight > 15 and self.weight < 20:
            return "Fat"
        else:
            return "Very Fat"
            
def main():
    my_thing = BigThing("balloon")
    print(my_thing.size())

    cutie = BigCat("mitzy", 22)
    print(cutie.size())

if __name__=="__main__":
    main()