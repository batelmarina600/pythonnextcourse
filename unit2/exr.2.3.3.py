class Bear:
    count_animals = 0

    def __init__(self, name="Beri"): #initialize
        self._name = name
        self._age = 0
        Bear.count_animals += 1
    
    def birthday(self):
        "adds one to the age"
        self._age += 1
    
    def get_age (self):
        "returns the self's age"
        return self._age
    
    def set_name(self, name):
        "sets the self's name"
        self._name = name

    def get_name(self):
        "returns the self's name"
        return self._name

def main():
    bear1 = Bear("Pandi")
    bear2 = Bear()

    print("bear1's name: ", bear1.get_name())
    print("bear2's name: ", bear2.get_name())

    bear2.set_name("Pooh")
    print("bear2's name: ", bear2.get_name())

    print(Bear.count_animals)

if __name__=="__main__":
    main()