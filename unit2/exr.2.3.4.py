class Pixel:
    "A class representing a pixel with its coordinates and RGB color values."
    def __init__(self, x=0, y=0, red=0, green=0, blue=0): 
        """ Initializes a new instance of the Pixel class.
        :param x: The x-coordinate of the pixel (default is 0).
        :type x: int
        :param y: The y-coordinate of the pixel (default is 0).
        :type y: int
        :param red: The red color value of the pixel (default is 0).
        :type red: int
        :param green: The green color value of the pixel (default is 0).
        :type green: int
        :param blue: The blue color value of the pixel (default is 0).
        :type blue: int
        """
        self._x = x
        self._y = y
        self._red = red
        self._green = green
        self._blue = blue

    def set_coords(self, x, y): 
        """ Sets the coordinates of the pixel.
        :param x: The x-coordinate of the pixel.
        :type x: int
        :param y: The y-coordinate of the pixel.
        :type y: int
        """
        self._x = x
        self._y = y

    def set_grayscale(self): 
        "Converts the pixel color to grayscale by averaging the RGB values."
        avg = self._red + self._green + self._blue
        avg = avg // 3
        self._red = avg
        self._green = avg
        self._blue = avg

    def print_pixel_info(self):
        "Prints information about the pixel, including its coordinates and color values."
        color = ""
        if(self._red == 0 and self._green == 0 and self._blue != 0):
            color = "Blue"
        if(self._red == 0 and self._green != 0 and self._blue == 0):
            color = "Green"
        if(self._red != 0 and self._green == 0 and self._blue == 0):
            color = "Red"

        print("X: " , self._x, ", Y: ", self._y, ", Color: (", self._red, "," , self._green ,"," , self._blue, ")", color)


def main():
    p = Pixel(5, 6, 250)
    p.print_pixel_info()
    p.set_grayscale()
    p.print_pixel_info()

if __name__=="__main__":
    main()